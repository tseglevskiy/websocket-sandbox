var WebSocketServer = require('ws').Server
  , http = require('http')
  , express = require('express')
  , app = express()
  , port = process.env.PORT || 5000;

app.use(express.static(__dirname + '/'));

var server = http.createServer(app);
server.listen(port);

console.log('http server listening on %d', port);

var wss = new WebSocketServer({server: server});
console.log('websocket server created');
wss.on('connection', function(ws) {
	ws.on('message', function(message, flags) {
		if (!flags.binary) {
			console.log('received: %s', message);
			ws.send('== ' + message.split("").reverse().join("") + ' ==');
		} else {
			console.log('received %d bytes of binary data', message.length);
			ws.send('== received ' + message.length + ' bytes of binary data ==');
		}
	});
	ws.send('something');
});

